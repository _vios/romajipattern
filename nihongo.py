#coding:utf8
import sys
import romajiConv

"""
ローマ字入力解決処理をするクラス
"""
class InputResolver:
    def __init__(self):
        self.test = 1
        self.word =""
        self.inputedIndex = 0

    """
    This word argument must be Python Unicode String
    """
    def setWord(self,word):
        self.word = word
    def getWord(self):
        return self.word

    def getNextRomajiList(self):
        #”じゃ”などの二文字入力ができるものについては
        #あとで実装する
        letter = self.word[self.inputedIndex]
        return romajiConv.convDict[letter]

    def incInputIndex(self):
        if self.inputedIndex + 1 >= len(self.word):
            return False
        else:
            self.inputedIndex += 1
            return True



r = open(sys.argv[1],"r")
for line in r:
    line = line.decode("utf8")
    for letter in line:
        print letter


inputResolver = InputResolver()
inputResolver.setWord(u"あいうえお")
print inputResolver.getWord()
ans = inputResolver.getNextRomajiList()
userAns = raw_input()
print userAns in ans
while inputResolver.incInputIndex():
    ans = inputResolver.getNextRomajiList()
    userAns = raw_input()
    print userAns in ans
#print inputResolver.test
